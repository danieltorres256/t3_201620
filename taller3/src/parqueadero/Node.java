package parqueadero;



public class Node <T>{

	private T item;
	private Node<T> next;
	

	public Node() {
		
		this.item = null;
		this.next = null;
	}
	
	
	public Node(T item, Node<T> next) {
		super();
		this.item = item;
		this.next = next;
	}
	
	
	
	public T getItem() {
		return item;
	}
	public void setItem(T item) {
		this.item = item;
	}
	public Node<T> getNext() {
		return next;
	}
	public void setNext(Node<T> next) {
		this.next = next;
	}
	

public static void main(String[] args){
		
		Node first = new Node();
		Node second = new Node();
		Node last = new Node();
		
		
		first.setItem("Hola");
		first.setNext(second);
		
		second.setItem("amigo");
		second.setNext(last);
		
		last.setItem("Chepe");
		
		
		for(Node x = first; x != null; x=x.getNext()){
			System.out.println(x.getItem());
		}
		
	}
	
}
