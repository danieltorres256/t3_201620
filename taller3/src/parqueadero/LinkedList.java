package parqueadero;



public class LinkedList<T> {

	private Node<T> first;
	private int size;
	
	public LinkedList(){
		first = null;
		size = 0;
	    
	}
	
	
	public Node<T> getFirst() {
		return first;
	}

	public int getSize(){
		return size;
	}
	
	public boolean isEmpty(){
		return size == 0;
	}
	
	public void add(T item){
		Node<T> current = first;
		Node<T> newNode = new Node<T>();
		newNode.setItem(item);
		
		
		if(first == null){
			first = newNode;
			size++;
			return;
		}
		
		while (current.getNext() != null){
			current =current.getNext();
		}
		current.setNext(newNode);
		size++;
	}
	
	
	
	public void insertAtK(T item, int k){
		Node<T> current = first;
	
		Node<T> newNode = new Node<T>();
		newNode.setItem(item);
		
		
		Node<T> temp = null;
	    if(k == 0){
	    	newNode.setNext(current);
	    	first = newNode;
	    	size++;
	    	return;
	    }
		
		int i = 0;
		while (i < k && current != null){
			temp = current;
			current =current.getNext();
			i++;
		}
		newNode.setNext(current);
		temp.setNext(newNode);
		size++;
		
	}
	
	public void deleteAtK(int k){
		Node<T> previous = null;

		if(k == 0){	    
	    	first = first.getNext();
	    	size--;
	    	return;
	    }
		
		int i = 0;
		Node<T> current = first;
		
		while (i < k && current != null){
		
			previous = current;
			current =current.getNext();
			
			i++;
		}
		previous.setNext(current.getNext());
		size--;
		
	}
	
	public static void main(String[] args){
		LinkedList<String> list = new LinkedList();
		list.add("Hola0");
		
		list.add("Hola1");
		list.add("Hola2");
		list.add("Hola3");
		list.add("Hola4");
		
		//list.insertAtK("Aja !!", 6);
		list.deleteAtK( 0);
		
		//Traverse the tree
		for(Node x = list.getFirst(); x != null; x=x.getNext()){
				System.out.println(x.getItem());
		}
	}

}
